# Introduction 
- Apps is created using Laravel 10.18.0 and php 8.2.8
- Clone the project
- in VS open terminal in project's folder
- install composer: `brew install composer`
- rename .env.example to .env
- install dependencies: `composer install`
- run server: `php artisan serve`


## Testing
- Test was perform using postman
- You can find collection in: storage/meeting_collection/Meeting.postman_collection.json


## Description
- Function is in: app/Models/Meeting.php:  createEvent is use to validate meeting data before create it and call scheduleMeeting if data is valid
