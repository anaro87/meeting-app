<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Meeting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MeetingRequest;
use DateTime;

class MeetingController extends Controller
{
     /**
     * create
     * 
     * @return json
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param array $userIds
     * @param string $name
     * @param dateTime $startDate
     * @param dateTime $endDate
     * 
     */
    public function create(Request $request){
        $meeting = New Meeting();
        $userIdArr = $request->get('userIds');
        $name = $request->get('name');
        $startDate = Carbon::parse($request->get('startDate'))->format('Y-m-d H:i:s'); 
        $endDate = Carbon::parse($request->get('endDate'))->format('Y-m-d H:i:s'); 

        $result = $meeting->createEvent($name, $startDate, $endDate, $userIdArr);
        
        return array("result" => $result);
    }
}
