<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class MeetingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'userIds' => 'required|array',
            'name' => 'required|string',
            'startDate' => 'required|date_format:Y-m-d H:i',
            'endDate' => 'required|date_format:Y-m-d H:i'
        ];
    }

    /**
     * Body params
     * @return void
     */
    public function bodyParameters()
    {
        return [
            'userIds' => [
                'description' => 'array of user ids',
                'example' => [
                    [1,2,3],
                ]
            ],
            'name' => [
                'description' => 'Meeting name',
                'example' => ["1-1"]
            ],
            'startDate' => [
                'description' => 'Meeting start date',
                'example' => ["2023-08-14 9:30"]
            ],
            'endDate' => [
                'description' => 'Meeting end date',
                'example' => ["2023-08-14 10:30"]
            ]
        ];
    }

    /**
     * throw error when validation fails
     *
     * @param Validator $validator
     * @return void
     */
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json(['errors' => $validator->errors()], 400)
        );
    }
}
