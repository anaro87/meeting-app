<?php

namespace App\Models;

use DateTime;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Meeting extends Model
{
    use HasFactory;
    protected $table = 'meeting';
    protected $primaryKey = 'id';

    /**
     *  Use to create validate meeting information before create it and schedule the meeting if data is ok
     *
     * @param [string] $meetingName
     * @param [dateTime] $startDate format 'Y-m-d H:i:s'
     * @param [dateTime] $endDate format Y-m-d H:i:s
     * @param [array] $userIds
     * @return json response
     */
    public function createEvent($meetingName, $startDate, $endDate, $userIds){
        $msg = 'The meeting has not been booked.';

        if(isset($meetingName) == false || isset($startDate) == false || isset($endDate) == false || isset($userIds) == false){
            $results["message"] = "Please verify that all values are set, {$msg}";
            return $results;
         }
        
        if($startDate > $endDate){
           $results["message"] = "Start Date can not be Greater that End Date, please verify your dates, {$msg}";
           return $results;
        }

        if($this->isValidDate($startDate) == false || $this->isValidDate($endDate) == false){
            $results["message"] = "You can not schedule meetings in the past , please verify your dates, {$msg}";
            return $results;
        }

        $conflicts= $this->validateAvailability($userIds, $startDate, $endDate);
        if(count($conflicts) > 0){
            foreach ($conflicts as $conflict) {
                $conflictUsers[] = "User {$conflict->user_id} Has conflict Meeting: {$conflict->meeting_name}";
            }

            $results["conflict_users"] = $conflictUsers;
            $results["message"] = 'Meeting has not been booked';
            
            return $results;
        }

        $wasScheduled = $this->scheduleMeeting($meetingName, $startDate, $endDate, $userIds);
        if($wasScheduled){
            $results["message"] = "The meeting has been successfully booked: {$meetingName} From:{$startDate} To:{$endDate}" ;
        }


        return $results;

    }

    /**
     * Schedule meeting, create the meeting in meeting table inf all validations are ok
     *
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param [string] $meetingName
     * @param [dateTime] $startDate format 'Y-m-d H:i:s'
     * @param [dateTime] $endDate format Y-m-d H:i:s
     * @param [array] $userIds
     * @return boolean
     */
    protected function scheduleMeeting($meetingName, $startDate, $endDate, $userIds): bool { 
        $result = false;

        foreach ($userIds as &$userId) {
            $cols = ['user_id' => $userId, 'start_time' => $startDate, 'end_time'=>$endDate, 'meeting_name'=>$meetingName];
            $result = DB::connection('mysql')->table('meeting')->insert($cols); 
        }

        return $result;
        
    }

    /**
     * get users that have conflict meetings
     *
     * @author Ana Rodriguez <anaro87@gmail.com>
     * @param [array] $userIds
     * @param [dateTime] $startDate format 'Y-m-d H:i:s'
     * @param [dateTime] $endDate format Y-m-d H:i:s
     * @return array
     */
    private function validateAvailability($userIds, $startDate, $endDate){
        $result =  DB::connection('mysql')
            ->table('meeting')
            ->select('id', 'meeting_name', 'user_id')
            ->where('end_time', '>=',$startDate)
            ->where('start_time','<=', $endDate)
            ->wherein('user_id', $userIds)
            ->get()->toArray();

        return $result;
    }

    /**
     * validate date with current date
     *
     * @param [dateTime] $date format 'Y-m-d H:i:s'
     * @return boolean
     */
    private function isValidDate($date){
        $now = Carbon::now()->format('Y-m-d H:i:s');
        $valid = $date < $now ? false: true;

        return $valid;

    }
}
